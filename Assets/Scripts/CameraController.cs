﻿using System.Collections;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject player;
    public Vector3 playerSpawn;
    public GameObject mainCamera;
    public GameObject musicObject;
    public float spawnDirection;
    public bool moveAllowed;

    public float currentScroll;
    public float currentX;
    private float currentY;

    private Rigidbody playerRb;
    private PlayerController playerFlags;
    private AudioSource currentAudio;
    private AudioSource musicAudio;
    private Vector3 zero;

    private void Start()
    {
        zero = new Vector3(0f, 0f, 0f);
        currentY = 35;
        currentScroll = -6;
        currentAudio = GetComponent<AudioSource>();

        playerRb = player.GetComponent<Rigidbody>();
        playerFlags = player.GetComponent<PlayerController>();
        musicAudio = musicObject.GetComponent<AudioSource>();
    }

    private void FixedUpdate()
    {
        if (moveAllowed)
        {
            if (playerFlags.enableStrafe)
            {
                currentX += Input.GetAxis("Mouse X");
            }
            else
            {
                currentX += Input.GetAxis("Mouse X") + (Input.GetAxis("Horizontal") * 3);
            }
            
            currentY += Input.GetAxis("Mouse Y") * - 1;
            currentScroll += Input.GetAxis("Mouse ScrollWheel") * 5;

            if (currentY > 90) currentY = 90;
            if (currentY < -50) currentY = -50;
            if (currentScroll > -1) currentScroll = -1;
            if (currentScroll < -51) currentScroll = -51;

            transform.position = player.transform.position;
            mainCamera.transform.localPosition = new Vector3(0f, 0f, currentScroll);

            transform.eulerAngles = new Vector3(currentY, currentX, 0f);
        }
        else
        {
            transform.LookAt(player.transform);
        }
    }

    public void BeginRespawn()
    {
        StartCoroutine(Respawn());
    }

    private IEnumerator Respawn()
    {
        musicAudio.Pause();
        player.gameObject.SetActive(false);
        currentAudio.Play();
        yield return new WaitForSecondsRealtime(1.5f);

        player.gameObject.SetActive(true);
        player.transform.position = playerSpawn;

        moveAllowed = true;
        currentX = spawnDirection;

        playerRb.isKinematic = true;
        yield return new WaitForSecondsRealtime(0.25f);
        player.gameObject.SetActive(false);
        yield return new WaitForSecondsRealtime(0.25f);
        player.gameObject.SetActive(true);
        yield return new WaitForSecondsRealtime(0.25f);
        player.gameObject.SetActive(false);
        yield return new WaitForSecondsRealtime(0.25f);
        player.gameObject.SetActive(true);
        playerRb.isKinematic = false;

        playerFlags.isDying = false;
        musicAudio.Play();
    }
}
