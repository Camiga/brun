﻿using UnityEngine;

public class BasherScript : MonoBehaviour
{
    private Animator move;

    private void Start()
    {
        move = gameObject.GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        move.SetFloat("RunFloat", Random.Range(0f, 1f));
    }
}
