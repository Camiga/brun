﻿using UnityEngine;

public class EndScript : MonoBehaviour
{
    public GameObject cameraGlider;
    public GameObject player;
    public Vector3 forceToAdd;
    public Vector3 cameraLocation;

    private CameraController cameraFlags;
    private PlayerController playerFlags;
    private AudioSource fireSound;
    private Rigidbody playerRb;
    private Vector3 zero;

    private void Start()
    {
        cameraFlags = cameraGlider.GetComponent<CameraController>();
        playerFlags = player.GetComponent<PlayerController>();
        playerRb = player.GetComponent<Rigidbody>();
        fireSound = GetComponent<AudioSource>();
        zero = new Vector3(0f, 0f, 0f);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            cameraFlags.moveAllowed = false;
            cameraFlags.currentScroll = 0;
            cameraGlider.transform.position = cameraLocation;

            playerFlags.canMove = false;
            playerRb.velocity = zero;
            playerRb.angularVelocity = zero;
            
            fireSound.Play();
            playerRb.AddForce(forceToAdd, ForceMode.Impulse);
        }
    }
}
