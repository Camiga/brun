﻿using UnityEngine;

public class TriggerRotator : MonoBehaviour
{
    public GameObject enableSpindle;
    public GameObject disableSpindle;
    private BarrelRotator enableFlags;
    private BarrelRotator disableFlags;
    private AudioSource enableSound;
    private AudioSource disableSound;

    private void Start()
    {
        enableFlags = enableSpindle.GetComponent<BarrelRotator>();
        enableSound = enableSpindle.GetComponent<AudioSource>();
        disableFlags = disableSpindle.GetComponent<BarrelRotator>();
        disableSound = disableSpindle.GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            enableFlags.canRotate = true;
            if (!enableSound.isPlaying) enableSound.Play();
            disableFlags.canRotate = false;
            disableSound.Pause();
        }
    }
}
