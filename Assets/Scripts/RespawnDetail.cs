﻿using UnityEngine;

public class RespawnDetail : MonoBehaviour
{
    public float fallTrigger;
    public float newDirection;
    public GameObject cameraGlider;
    public GameObject particles;
    private CameraController cameraFlags;
    private Vector3 newSpawn;

    private void Start()
    {
        newSpawn = transform.position + new Vector3(0f, 3f, 0f);
        cameraFlags = cameraGlider.GetComponent<CameraController>();
    }

    private void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            PlayerController playerScript = col.gameObject.GetComponent<PlayerController>();

            playerScript.deathHeight = fallTrigger;
            cameraFlags.playerSpawn = newSpawn;
            cameraFlags.spawnDirection = newDirection;

            Instantiate(particles, transform.position, Quaternion.Euler(-90, 0, 0));
            gameObject.SetActive(false);
        }
    }
}
