﻿using UnityEngine;

public class GameFinish : MonoBehaviour
{
    public GameObject player;
    public GameObject menuText;
    public GameObject endCanvas;
    private TMPro.TextMeshProUGUI completeText;
    private Rigidbody playerRb;
    private AudioSource deathSound;

    private void Start()
    {
        playerRb = player.GetComponent<Rigidbody>();
        deathSound = GetComponent<AudioSource>();
        completeText = menuText.GetComponent<TMPro.TextMeshProUGUI>();
    }

    private void SlowTime() 
    {
        if (Time.timeScale - 0.1f < 0f)
        {
            endCanvas.SetActive(true);
            Time.timeScale = 0f;
        }
        if (Time.timeScale > 0) Time.timeScale -= 0.1f;
    }
    private void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            completeText.text = "Return to Menu\nafter " + Time.timeSinceLevelLoad.ToString("n2") + " seconds";
            playerRb.drag = 5f;
            playerRb.AddForce(-100f, 0f, 0f);
            deathSound.Play();
            InvokeRepeating("SlowTime", 1f, 0.1f);
        }
    }
}
