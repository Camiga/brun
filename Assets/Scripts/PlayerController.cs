﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public bool canMove;
    public bool isDying;
    public bool enableStrafe;
    public GameObject mainCamera;
    public GameObject cameraGlider;
    public float deathHeight;

    private Rigidbody rb;
    private CameraController cr;
    private Vector3 movement;
    private Vector3 zero;
    
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        cr = cameraGlider.GetComponent<CameraController>();
        deathHeight = transform.position.x - 20;

        zero = new Vector3(0f, 0f, 0f);
        canMove = true;
        isDying = false;
    }

    private void FixedUpdate()
    {
        if (transform.position.y <= deathHeight)
        {
            isDying = true;
            cr.moveAllowed = false;
            rb.AddForce(new Vector3(0f, -500f, 0f));
        }

        if (canMove && !isDying)
        {
            float moveVertical = Input.GetAxis("Vertical");
            
            if (enableStrafe)
            {
                float moveHorizontal = Input.GetAxis("Horizontal");
                movement = new Vector3(moveHorizontal / 1.5f, 0f, moveVertical);
            }
            else
            {
                movement = new Vector3(0f, 0f, moveVertical);
            }

            Vector3 relativeMovement = mainCamera.transform.TransformVector(movement);
            Vector3 straightMovement = new Vector3(relativeMovement.x, 0, relativeMovement.z);

            rb.AddForce(straightMovement * speed);
        }
    }

    private void OnCollisionEnter(Collision col)
    {
        if (isDying)
        {
            rb.velocity = zero;
            rb.angularVelocity = zero;
            cr.BeginRespawn();
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("GameController"))
        {
            AudioSource ad = col.gameObject.GetComponent<AudioSource>();
            enableStrafe = !enableStrafe;
            ad.Play();
        }
    }
}
