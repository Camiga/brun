﻿using UnityEngine;

public class FPSCounter : MonoBehaviour
{
    private float deltaTime;

    private void Start()
    {
        InvokeRepeating("UpdateClock", 0.5f, 0.5f);
        deltaTime += Time.unscaledDeltaTime - deltaTime;
    }

    private void UpdateClock() => deltaTime += Time.unscaledDeltaTime - deltaTime;

    private void OnGUI()
    {
        GUIStyle style = new GUIStyle
        {
            alignment = TextAnchor.UpperRight,
            fontSize = Screen.height * 4 / 100
        };

        style.normal.textColor = new Color(1, 1, 1, 1);

        GUI.Label(
            new Rect(0, 0, Screen.width, Screen.height * 5 / 100),
            string.Format("{1:0.} fps ({0:0.00} ms)", deltaTime * 1000, 1 / deltaTime),
            style);
    }
}
