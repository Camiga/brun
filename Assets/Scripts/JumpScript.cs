﻿using UnityEngine;

public class JumpScript : MonoBehaviour
{
    public Vector3 forceToAdd;
    private AudioSource sound;
    private Rigidbody otherRb;

    private void Start()
    {
        sound = GetComponent<AudioSource>();
    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            otherRb = other.gameObject.GetComponent<Rigidbody>();
            otherRb.AddForce(forceToAdd, ForceMode.Impulse);
            sound.Play();
        }
    }
}
