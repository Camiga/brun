﻿using UnityEngine;

public class BarrelRotator : MonoBehaviour
{
    public bool canRotate;
    public Vector3 rotateIncrement;
    
    private void Start()
    {
        canRotate = false;
    }

    private void FixedUpdate()
    {
        if (canRotate)
        {
            gameObject.transform.Rotate(rotateIncrement);
        }
    }
}
